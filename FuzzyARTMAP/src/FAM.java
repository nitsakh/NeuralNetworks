import java.util.ArrayList;
import java.util.Arrays;


public class FAM {

	Double baselineVigilance = 0.4;
	Double choice = 0.01;
	
	ArrayList<CatNode> catnodes;
	
	//Default Constructor
	public FAM(){
		
	}
	
	//Parameterized Constructor to accept Rho_a_bar and choice parameter
	public FAM(Double bVig,Double ch){
		baselineVigilance = bVig;
		choice = ch;
	}
	
	public int Train(ArrayList<Double []> ins, ArrayList<Double []> outs, int maxepochs){
		
		//To determine number of epochs
 		int epochs = 0;
		//To determine whether the weights have been updated in current epoch
		boolean weightsUpdated = true;
		//List of Category Layer nodes.
		catnodes = new ArrayList<CatNode>();
		
		int inlen = ins.get(0).length;
		int outlen = outs.get(0).length;
		
		//Start with one uncommitted node initially
		catnodes.add(new CatNode(inlen, outlen));
		
		while(weightsUpdated && epochs < maxepochs){
			epochs++;
			weightsUpdated = false;
			for(int pat = 0;pat<ins.size();pat++){
			
				//Set the vigilance to baseline vigilance initially
				Double vigilance=baselineVigilance;
				
				Double [] inp = ins.get(pat);
				Double [] outp = outs.get(pat);
				
				//Create Input and Output nodes with corresponding 
				//component values
				InNode [] in= new InNode[inlen];
				OutNode [] out= new OutNode[outlen];
				for(int i=0;i<in.length;i++){
					in[i]=new InNode(inp[i]);
				}
				
				for(int i=0;i<out.length;i++){
					out[i]=new OutNode(outp[i]);
				}
				
				for(CatNode n:catnodes){
					calculateBUInput(n,in);
				}
				CatNode jmax;
				int jmaxindex;
				while(true){
					while(true){
						//Choose the node that has maximum bottom up input
						jmaxindex = getMaxIndex(catnodes);
						jmax = catnodes.get(jmaxindex);
						
						//Check to see if this node satisfies vigilance criterion
						//We consider 3 cases for this
						
						//Node is uncommitted
						if(!jmax.isComm){
							//This means it satisfies Vigilance Criterion
							//So, don't do anything.
							break;
						}
						else{
							//Check if node satisfies Vigilance Criterion
							if(satisfiesVigilanceCriterion(in,jmax,vigilance)){
								//Do nothing, goto next step
								break;
							}
							//Does not satisfy vigilance criterion
							else{
								//Set the bottom up input to -1
								jmax.bui = -1.0;
								continue;
							}
						}
					}
					
					//Step 5
					//If node is uncommitted
					if(!jmax.isComm){
						int index = getOutputIndex(out);
						//Set the weight corresponding to the output as 1
						jmax.iawts[index] = 1d;
						
						//Update the top down weights
						jmax.tdwts = fuzzyMinArr(in, jmax.tdwts);
						jmax.isComm = true;
						//Add new uncommitted node
						catnodes.add(new CatNode(in.length, out.length));
						weightsUpdated = true;
						break;
					}
					//Else if node is committed
					else{
						if(isMatch(out,jmax.iawts)){
							//Update the top down weights
							Double [] newwts = fuzzyMinArr(in, jmax.tdwts);
							if(!Arrays.equals(jmax.tdwts,newwts)){
								jmax.tdwts = newwts;
								weightsUpdated = true;
							}
							break;
						}
						//if Inter ART weight vector is different than Output
						else{
							//Reset node jmax
							jmax.bui = -1d;
							
							//Increase vigilance level
							vigilance = fuzzyMin(in, jmax.tdwts)/sumArr(inp);
							continue;
						}
					}
				}
			}
		}
		sortCat();
		return epochs;
	}

	//Sorting Category List
	public void sortCat(){
		ArrayList<ArrayList<CatNode>> sortedcats = new ArrayList<ArrayList<CatNode>>();
		ArrayList<CatNode> tempgene = new ArrayList<CatNode>(catnodes);
		while(!tempgene.isEmpty()){
			CatNode cn1 = tempgene.get(0);
			ArrayList<CatNode> temp= new ArrayList<CatNode>();
			temp.add(cn1);
			int i=1;
			while(i<tempgene.size()){
				CatNode cn2 = tempgene.get(i++);
				if(Arrays.equals(cn1.iawts,cn2.iawts)){
					temp.add(cn2);
					tempgene.remove(cn2);
					i--;
				}
			}
			tempgene.remove(0);
			sortedcats.add(temp);
		}
		catnodes = null;
		catnodes = new ArrayList<CatNode>();
		for(ArrayList<CatNode> catl:sortedcats){
			catnodes.addAll(catl);
		}
	}
	//Performance Phase of Fuzzy ARTMAP
	public ArrayList<Double []> Test(ArrayList<Double []> ins,ArrayList<Double []> actualouts){
		
		ArrayList<CatNode> cattest = new ArrayList<CatNode>();
		
		ArrayList<Double []> outs = new ArrayList<Double[]>();
		
		//Create Input and Output nodes with corresponding 
		//component values
		int inlen = ins.get(0).length;
		int outlen = catnodes.get(0).iawts.length;
		
		for(int pat = 0;pat<ins.size();pat++){
			
			cattest.clear();
			//Set the vigilance to baseline vigilance initially
			Double vigilance=baselineVigilance;
			
			Double [] inp = ins.get(pat);
			InNode [] in= new InNode[inlen];
			for(int i=0;i<in.length;i++){
				in[i]=new InNode(inp[i]);
			}
			
			OutNode [] out= new OutNode[outlen];
			
			//Calculate the bottom up inputs, and copying the Category
			//nodes to a new List
			for(CatNode n:catnodes){
				CatNode c = new CatNode(n);
				calculateBUInput(c,in);
				cattest.add(c);
			}
			CatNode jmax;
			int jmaxindex;
		
			while(true){
				//Choose the node that has maximum bottom up input
				jmaxindex = getMaxIndex(cattest);
				jmax = cattest.get(jmaxindex);
				
				//Check to see if this node satisfies vigilance criterion
				//We consider 3 cases for this
				
				//Node is uncommitted
				if(jmax.isComm == false){
					//This means it satisfies Vigilance Criterion
					//So, don't do anything.
					break;
				}
				else{
					//Check if node satisfies Vigilance Criterion
					if(satisfiesVigilanceCriterion(in,jmax,vigilance)){
						//Do nothing, goto next step
						break;
					}
					//Does not satisfy vigilance criterion
					else{
						//Set the bottom up input to -1
						jmax.bui = -1.0;
						continue;
					}
				}
			}
			
			//Step 5
			//If node is uncommitted
			if(!jmax.isComm){
				
				//Set output as unknown
				for(int i=0;i<out.length;i++){
					out[i] = new OutNode(99.0);
				}
			}
			//Else if node is committed
			else{
				//Set the Inter ART vector of this jmax as the output
				for(int i = 0;i<jmax.iawts.length;i++){
					out[i] = new OutNode(jmax.iawts[i]);
				}
				if(Arrays.equals(jmax.iawts, actualouts.get(pat))){
					catnodes.get(jmaxindex).chosencorrect+=1;
				}
				catnodes.get(jmaxindex).chosen +=1;
			}
			
			//Add the values to the outputs list
			Double op [] = new Double[out.length];
			try{
			for(int i=0;i<out.length;i++){
				op[i] = out[i].value;
			}
			}
			catch(Exception e){
				System.out.println(e.getStackTrace());
			}
			outs.add(op);
		}
		return outs;
	}
	
	public void calculateBUInput(CatNode n,InNode [] in){
		n.bui = fuzzyMin(in, n.tdwts)/(choice+sumArr(n.tdwts));
	}
	
	//Implements the Fuzzy Min operator
	//This calculates the minimum between two corresponding
	//components of vector and then determines the sum. 

	public double fuzzyMin(InNode [] in,Double [] tdwts){
		Double sum = 0d;
		Double [] result = fuzzyMinArr(in,tdwts);
		sum = sumArr(result);
		return sum;
	}
	
	public Double [] fuzzyMinArr(InNode [] in,Double [] tdwts){
		Double result[]=new Double[tdwts.length];
		for(int i=0;i<in.length;i++){
			result[i]=Math.min(tdwts[i], in[i].value);
		}
		return result;
	}
	
	//Calculate sum of all array values
	public double sumArr(Double arr[]){
		Double sum=0d;
		//Sum all values in array
		for(Double d:arr){
			sum+=d;
		}
		return sum;
	}

	//This method determines the index of CatNode having maximum
	//bottom up input.
	public int getMaxIndex(ArrayList<CatNode> cn){
		int index=0,cnt=-1;
		Double max=-999999d;
		for(CatNode n:cn){
			cnt++;
			if(n.bui>max){
				max = n.bui;
				index = cnt;
			}
		}
		return index;
	}
	
	//Check if node satisfies the vigilance criterion
	public boolean satisfiesVigilanceCriterion(InNode [] in,CatNode n, Double vig){
		boolean satisfies = false;
		Double [] inval = new Double[in.length];
		for(int i = 0;i<in.length;i++){
			inval[i] = in[i].value;
		}
		Double lhs = fuzzyMin(in, n.tdwts)/sumArr(inval);
		
		satisfies = (lhs>=vig);
		return satisfies;
	}
	
	//Get index of 1 in the output
	public int getOutputIndex(OutNode [] out){
		int index=0;
		while(out[index].value != 1)
			index++;
		return index;
	}
	
	//Function isMatch determines if the output vector matches the 
	//Inter ART weights
	public boolean isMatch(OutNode [] out, Double [] iawts){
		boolean isM = true;
		for(int i = 0;i<out.length;i++){
			if(!out[i].value.equals(iawts[i])){
				isM = false;
				break;
			}
		}
		return isM;
	}
	
	//Function to return the number of Category Nodes or size of network
	public int getSize(){
		return catnodes.size();
	}
	
	//Function to return the list of Category Nodes
	public ArrayList<CatNode> getCatNodes(){
		return catnodes;
	}
	
	//Function to set the Category Nodes for the FAM
	public void setCatNodes(ArrayList<CatNode> cnodes){
		catnodes = cnodes;
	}
}
