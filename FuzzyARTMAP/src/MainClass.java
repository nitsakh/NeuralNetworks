import java.io.IOException;
import java.util.ArrayList;


public class MainClass {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		ArrayList<Double []> input= new ArrayList<Double []>();
		/*input.add(new Double[]{0.3,0.2,0.7,0.8});
		input.add(new Double[]{0.6,0.7,0.4,0.3});
		input.add(new Double[]{0.2,0.1,0.8,0.9});
		input.add(new Double[]{0.25,0.25,0.75,0.75});
		input.add(new Double[]{0.8,0.85,0.2,0.15});
		input.add(new Double[]{0.28,0.5,0.72,0.5});*/
		
		ArrayList<Double []> output = new ArrayList<Double[]>();
		/*output.add(new Double[]{1.0,0.0});
		output.add(new Double[]{1.0,0.0});
		output.add(new Double[]{1.0,0.0});
		output.add(new Double[]{0.0,1.0});
		output.add(new Double[]{1.0,0.0});
		output.add(new Double[]{0.0,1.0});*/
		
		/*ArrayList<Double []> intest = new ArrayList<Double[]>();
		
		intest.add(new Double[]{0.6,0.7,0.4,0.3});
		intest.add(new Double[]{0.2,0.1,0.8,0.9});
		intest.add(new Double[]{0.25,0.25,0.75,0.75});
		intest.add(new Double[]{0.8,0.85,0.2,0.15});
		intest.add(new Double[]{0.28,0.5,0.72,0.5});*/
		
		
		DataManager dm =new DataManager(input, output);
		//0 - Training, 1 - Validation, 2 - Testing
		String dataset = "g4c_25";
		dm.getData(dataset, 0);
		
		int EPOCHS = 100;
		FAM fa = new FAM();
		int epoch = fa.Train(input, output, EPOCHS);
		
		System.out.println("Number of Epochs to Train: "+epoch);
		System.out.println("Size of Network: "+fa.getSize());
		dm.getData(dataset, 2);
		ArrayList<Double []> newoutput = fa.Test(input,output);
		
		System.out.println("Accuracy : "+dm.getAccuracy(output, newoutput));
		
		/*for(Double [] o: newoutput){
			for(Double d: o){
				System.out.print(d+" ");
			}
			System.out.println("\n");
		}*/

	}

}
