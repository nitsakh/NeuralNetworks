This program implements the Fuzzy ARTMAP neural network (grossberg et al.),
including the training and the performance (testing) phases.
Variable in the MainClass.java can be modified to determine the
dataset for use currently. 