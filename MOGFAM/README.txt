The Multi Objective Genetically Optimized Fuzzy ARTMAP (MOGFAM) program implementation
depends upon the Fuzzy ARTMAP program present in the repo. The Datasets are included
in the FuzzyARTMAP program sources. The dataset to use for current execution of the
program is set through a static variable at the beginning of the GeneticAlgorithm.java file.