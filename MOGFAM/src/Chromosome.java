import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class Chromosome {
	
	ArrayList<CatNode> genes = new ArrayList<CatNode>();
	ArrayList<CatGrp> ggrp = new ArrayList<CatGrp>();
	Double error = 0d;
	int complexity = 0;
	int strength = 0;
	int Rx = 0;
	
	Double fitness = 0d;
	
	//Copy the chromosome
	public Chromosome(Chromosome ch) throws IOException{
		this.error = ch.error;
		this.complexity = ch.complexity;
		this.strength = ch.strength;
		this.fitness = ch.fitness;
		for(CatNode c:ch.genes){
			genes.add(new CatNode(c));
		}
		for(CatGrp cg:ch.ggrp){
			ggrp.add(new CatGrp(cg));
		}
	}
	
	//Evaluate
	public void evaluate() throws IOException{
		calcError();
		groupCats();
		complexity = genes.size();
	}
	
	//Creates a new chromosome and calculates error
	public Chromosome(ArrayList<CatNode> cnodes) throws IOException{
		for(CatNode c:cnodes){
			genes.add(new CatNode(c));
		}
		calcError();
		groupCats();
		complexity = genes.size();
	}
	
	//Function to calculate distance between two chromosomes
	public Double getDistance(Chromosome ch){
		if(this == ch)
			return 999999999d;
		Double comp = Math.pow(ch.complexity-this.complexity,2);
		Double stren = Math.pow(ch.strength-this.strength,2);
		return Math.sqrt(stren+comp);
	}
	
	//Separating different categories into different arraylists
	public void groupCats(){	
		ArrayList<CatGrp> catlis = new ArrayList<CatGrp>();
		ArrayList<CatNode> tempgene = new ArrayList<CatNode>();
		ArrayList<CatNode> newtemp = new ArrayList<CatNode>();
		for(CatNode c:genes){
			tempgene.add(c);
		}
		while(!tempgene.isEmpty()){
			CatNode cn1 = tempgene.get(0);
			ArrayList<CatNode> temp= new ArrayList<CatNode>();
			temp.add(cn1);
			int i=1;
			while(i<tempgene.size()){
				CatNode cn2 = tempgene.get(i++);
				if(Arrays.equals(cn1.iawts,cn2.iawts)){
					temp.add(cn2);
					tempgene.remove(cn2);
					i--;
				}
				else
					break;
			}
			tempgene.remove(cn1);
			catlis.add(new CatGrp(temp));
			newtemp.addAll(temp);
		}
		Collections.sort(catlis);
		ggrp = catlis;
		genes = newtemp;
	}
	
	public int getComplexity(){
		return genes.size();
	}
	
	public Double getError(String dataset){
		return error;
	}
	
	public void setStrength(int strength){
		this.strength = strength;
	}
	
	public int getStrength(){
		return strength;
	}
	
	public void setRx(int r){
		Rx = r;
	}
	
	public int getRx(){
		return Rx;
	}
	
	public void setFitness(Double f){
		this.fitness = f;
	}
	
	public Double getFitness(){
		return fitness;
	}
	
	//Function to calculate error and store in error variable
	public void calcError() throws IOException{
		ArrayList<Double []> in = new ArrayList<Double[]>();
		ArrayList<Double []> out = new ArrayList<Double[]>();
		DataManager dm =new DataManager(in, out);
		dm.getData(GeneticAlgorithm.dataset, 1);
		FAM fa = new FAM();
		fa.setCatNodes(genes);
		ArrayList<Double []> newout = fa.Test(in,out);
		error = 1- dm.getAccuracy(out,newout);
	}
	
	//determines if current dominates the other individual
	public boolean doesDominate(Chromosome other){
		if(this == other)
			return true;
		if(this.error < other.error && this.complexity < other.complexity){
			return true;
		}
		else if(this.error.equals(other.error)){
			return this.complexity < other.complexity;
		}
		else if(this.complexity == other.complexity){
			return this.error < other.error;
		}
		return false;
	}
}
