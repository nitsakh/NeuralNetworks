import java.util.ArrayList;
import java.util.Random;


public class CatGrp implements Comparable<CatGrp>{

	ArrayList<CatNode> nodes ;
	Integer id = 0;
	
	Random rnd = new Random();
	
	public CatGrp(CatGrp cg){
		nodes = new ArrayList<CatNode>();
		for(CatNode cn:cg.nodes){
			nodes.add(new CatNode(cn));
		}
		id = cg.id;
	}
	
	public CatGrp(ArrayList<CatNode> n){
		nodes = n;
		CatNode temp = n.get(0);
		String bin = "";
		for(Double d:temp.iawts){
			bin = bin + String.valueOf(Math.round(d));
		}
		id = Integer.parseInt(bin, 2);
	}

	//Prune nodes from the category group
	public void prune(Chromosome ch){
		ArrayList<CatNode> delnodes = new ArrayList<CatNode>();
		for(CatNode c:nodes){
			if(delnodes.size()<nodes.size()-1)
				if(rnd.nextDouble()<(1-c.CF)){
					delnodes.add(c);
				}
		}
		nodes.removeAll(delnodes);
		ch.genes.removeAll(delnodes);
	}
	
	//Crossover between same categories
	public void crossover(CatGrp other){
		int ind1 = rnd.nextInt(nodes.size());
		int ind2 = rnd.nextInt(other.nodes.size());
		
		ArrayList<CatNode> t1=new ArrayList<CatNode>();
		ArrayList<CatNode> t2 = new ArrayList<CatNode>();
		
		for(int i=0;i<ind1;i++){
			t1.add(nodes.get(i));
		}
		
		for(int i=ind2;i<other.nodes.size();i++){
			t1.add(other.nodes.get(i));
		}
		
		for(int i=0;i<ind2;i++){
			t2.add(other.nodes.get(i));
		}
		
		for(int i=ind1;i<nodes.size();i++){
			t2.add(nodes.get(i));
		}
		this.nodes = t1;
		other.nodes = t2;
	}
	@Override
	public int compareTo(CatGrp o) {
		return id.compareTo(o.id);
	}
}
