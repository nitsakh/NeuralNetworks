import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class DataManager {
	
	ArrayList<Double []> inputs = new ArrayList<Double []>();
	ArrayList<Double []> outputs = new ArrayList<Double []>();
	
	public DataManager(ArrayList<Double []> i,ArrayList<Double []> o){
		inputs = i;
		outputs = o;
	}
	
	public void getData(String dname,int val) throws IOException{
		inputs.clear();
		outputs.clear();
		
		String path = "../FuzzyARTMAP/Datasets/"+dname+"/";
		String fname = getFileName(dname,val);
		
		BufferedReader br = new BufferedReader(new FileReader(path+fname));
		ArrayList<String> lines = new ArrayList<String>();
		
		//Read and add all lines to the arraylist
		String temp;
		while((temp = br.readLine())!=null){
			lines.add(temp);
		}
		
		//Randomly Shuffle the rows in the dataset
		Collections.shuffle(lines);
		
		//Read values into the input and output lists
		for(String line:lines){
			String [] spl = line.split(",");
			String in = spl[0];
			String out = spl[1];
			
			String [] invals = in.split(" ");
			//Array after converted to double
			//The *2 is to consider complementary encoding of inputs
			Double [] dinvals = new Double [invals.length*2];
			
			//Convert each string into double and add to inputs list
			for(int i=0;i<invals.length;i++){
				dinvals[i]=Double.parseDouble(invals[i]);
				//Adding the complement of the current value
				dinvals[i+invals.length] = 1-dinvals[i];
			}
			
			inputs.add(dinvals);
			
			String [] outvals = out.split(" ");
			Double [] doutvals = new Double [outvals.length];
			
			//Convert each string into double and add to outputs list
			for(int i=0;i<outvals.length;i++){
				doutvals[i]=Double.parseDouble(outvals[i]);
			}
			outputs.add(doutvals);
		}
		br.close();
	}
	
	public Double getAccuracy(ArrayList<Double []> actual,ArrayList<Double []> result){
		Double accuracy=0d;
		int count = 0;
		
		for(int i=0;i<actual.size();i++){
			if(Arrays.equals(actual.get(i), result.get(i))){
				count++;
			}
		}
		accuracy = count*1d/actual.size();
		
		return accuracy;
	}
	
	public String getFileName(String dname,int i){
		String fname = "";
		
		switch(i){
		//Training File
		case 0:
			fname = dname+"Training.txt";
			break;
		//Validation File
		case 1:
			fname = dname+"Validation.txt";
			break;
		//Testing File
		case 2:
			fname = dname+"Testing.txt";
			break;
		}
		
		return fname;
	}
}
